package com.eiricha.base;

import java.io.PrintWriter;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class LoggerTest {

   private final ArrayList<String> messages = new ArrayList<>();

   class MockWriter extends PrintWriter {
      public MockWriter() {
         super(System.out);
      }

      @Override
      public void println(String s) {
         messages.add(s);
      }
   }

   @Test
   public void log_currentLevel_isLowerThan_parameterLevel() {
      Logger logger = new LoggerImpl(Level.INFO, new MockWriter());
      logger.log(Level.WARN, "log_currentLevel_isLowerThan_parameterLevel");

      Assert.assertEquals(1, messages.size());
      Assert.assertEquals("log_currentLevel_isLowerThan_parameterLevel", messages.get(0));
   }

   @Test
   public void log_currentLevel_isEqualTo_parameterLevel() {
      Logger logger = new LoggerImpl(Level.INFO, new MockWriter());
      logger.log(Level.INFO, "log_currentLevel_isEqualTo_parameterLevel");

      Assert.assertEquals(1, messages.size());
      Assert.assertEquals("log_currentLevel_isEqualTo_parameterLevel", messages.get(0));
   }

   @Test
   public void log_levelIsAll() {
      Logger logger = new LoggerImpl(Level.ALL, new MockWriter());
      logger.log(Level.WARN, "log_levelIsAll");

      Assert.assertEquals(1, messages.size());
      Assert.assertEquals("log_levelIsAll", messages.get(0));
   }

   @Test
   public void log_currentLevel_isNone() {
      Logger logger = new LoggerImpl(Level.NONE, new MockWriter());
      logger.log(Level.WARN, "log_levelIsAll");

      Assert.assertEquals(0, messages.size());
   }
}
