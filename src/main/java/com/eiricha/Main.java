package com.eiricha;

import com.eiricha.base.Level;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
   public static void main(String[] args) {
      Level l1 = Level.INFO;
      Level l2 = Level.WARN;

      System.out.println(l1.getPriority() <= l2.getPriority());
   }
}