package com.eiricha.base;

import java.io.PrintWriter;

public interface Logger {

   void setCurrentLevel(Level currentLevel);

   Level getCurrentLevel();

   void setWriter(PrintWriter writer);

   PrintWriter getWriter();

   void log(Level level, String msg);
}
