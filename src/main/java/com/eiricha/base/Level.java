package com.eiricha.base;

public enum Level {
   NONE(1),
   INFO(2),
   WARN(3),
   ERROR(4),
   ALL(10);

   private final int priority;

   Level(int priority) {
      this.priority = priority;
   }

   public int getPriority() {
      return priority;
   }
}
