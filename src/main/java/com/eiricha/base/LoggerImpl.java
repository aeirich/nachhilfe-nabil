package com.eiricha.base;

import java.io.PrintWriter;

public class LoggerImpl implements Logger {

   private Level currentLevel;
   private PrintWriter printWriter;

   public LoggerImpl(Level currentLevel, PrintWriter printWriter) {
      this.currentLevel = currentLevel;
      this.printWriter = printWriter;
   }

   @Override
   public void setCurrentLevel(Level currentLevel) {
      this.currentLevel = currentLevel;
   }

   @Override
   public Level getCurrentLevel() {
      return currentLevel;
   }

   @Override
   public void setWriter(PrintWriter writer) {
      this.printWriter = writer;
   }

   @Override
   public PrintWriter getWriter() {
      return printWriter;
   }

   @Override
   public void log(Level level, String msg) {
      Level currentLevel = getCurrentLevel();
      if (currentLevel != Level.NONE) {
         if (currentLevel == Level.ALL) {
            printWriter.println(msg);
         } else {
            if (currentLevel.getPriority() <= level.getPriority()) {
               printWriter.println(msg);
            }
         }
      }
   }
}
